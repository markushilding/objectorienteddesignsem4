package integration;

import model.Receipt;

/*
 * Printing receipts
 */
public class Printer {
	
	/**
	 * Prints the receipt with object from Receipt
	 * class.
	 * @param info
	 */
	public void printReceipt(Receipt receipt) {
		System.out.println(receipt.getReceipt());
	}
}
