package integration;

public class ItemNotFoundException extends Exception {
	
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	public ItemNotFoundException(int id){
		super("Item with identifier " + id + " not found.\n");
	}
	
}
