package integration;

/*
 * Stores info about an item.
 */
public class ItemDTO {
	private final int itemId;
	private final String name;
	private final double price;
	private final double taxRate;
	
	/**
	 * Constructor
	 * @param itemId	item identifier.
	 * @param name		item name.
	 * @param price		item price excl. taxes.
	 * @param taxRate	tax rate in decimal form.
	 */
	public ItemDTO(int itemId, String name, double price, double taxRate){
		this.itemId = itemId;
		this.name = name;
		this.price = price;
		this.taxRate = taxRate;
	}
	
	/**
	 * Gets item id
	 * @return itemId
	 */
	public int getItemId(){
		return itemId;
	}
	
	/**
	 * Gets item name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets item price
	 * @return price
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Gets tax rate
	 * @return taxRate
	 */
	public double getTaxRate() {
		return taxRate;
	}
	
	/**
	 * Printing the item results in "name	price"
	 */
	@Override
	public String toString() {
		return name + "\t" + price;
	}
	
}
