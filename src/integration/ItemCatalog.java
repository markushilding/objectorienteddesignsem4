package integration;

import java.sql.SQLException;

import model.SaleDTO;

/*
 * Works as fictional database and contains the
 * products that are in store with their id, name,
 * price and tax rate.
 */
public class ItemCatalog{
	
	
	private static final ItemDTO[] itemsInStore = {
		new ItemDTO(1,"Banana", 8.99, 0.12),
		new ItemDTO(2,"Apple", 5.68, 0.12),
		new ItemDTO(3,"Orange", 12.19, 0.12),
		new ItemDTO(4,"Milk", 8, 0.12),
		new ItemDTO(5,"Juice", 24.50, 0.12),
		new ItemDTO(6,"Bread", 5, 0.12),
		new ItemDTO(7,"Pizza", 45, 0.12),
		new ItemDTO(8,"Sausage", 25, 0.25),
		new ItemDTO(9,"Coffee", 50, 0.25),
		new ItemDTO(10,"Tea", 27, 0.25),
		new ItemDTO(11,"Potatoes", 3, 0.25),
		new ItemDTO(12,"Rice", 25, 0.25),
		new ItemDTO(13,"Pasta", 20, 0.25),
		new ItemDTO(14,"Salami", 29, 0.25),
		new ItemDTO(15,"Gum", 14, 0.25),
		new ItemDTO(16,"Corn Flakes", 36.60, 0.25),
		new ItemDTO(17,"Soap", 24.23, 0.25),
		new ItemDTO(18,"Toilet paper", 40, 0.25),
		new ItemDTO(19,"Salmon", 120, 0.25),
		new ItemDTO(20,"Curry", 30, 0.25),
		new ItemDTO(21,"Salt", 22.30, 0.25),
		new ItemDTO(22,"Pepper", 19.20, 0.25),
		new ItemDTO(23,"Onion", 8, 0.25)
	};
	
	
	/**
	 * Search for item in the catalog and returns
	 * the item object if it exists.
	 * @param id 	Given item identifier.
	 * @return 		ItemDTO
	 * @throws SQLException something went wrong calling database.
	 * @throws ItemNotFoundException item with given identifier not found. 
	 */
	public ItemDTO search(int id) throws SQLException, ItemNotFoundException{
		
		if(id == 7) throw new SQLException();
		
		for(ItemDTO item : itemsInStore) { 
			if(item.getItemId() == id) return item;
		}
		throw new ItemNotFoundException(id);
	}
	
	/**
	 * Should log data to external systems.
	 * @param salesData Includes all info about sale.
	 */
	public void logSale(SaleDTO salesData) {
		
	}
}

