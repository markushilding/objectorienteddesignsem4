package integration;

import static org.junit.Assert.*;
import org.junit.Test;

import model.OperationFailedException;
import model.Sale;


public class ItemNotFoundExceptionTest {
	
	@Test
	public void testException() throws ItemNotFoundException, OperationFailedException{
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		
		try {
			ItemDTO i = sale.addItem(100, 1);
		}
		catch(ItemNotFoundException infException) {
			return;
		}
		catch(OperationFailedException ofException) {
			fail("OperationFailedException occurred.");
		}
		
		fail("Try statement should fail for item id 100.");
	}
}
