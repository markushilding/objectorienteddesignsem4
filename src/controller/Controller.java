package controller;

import integration.*;
import model.*;

/*
 * Handles calls from view and calls model class functions
 * to handle the logic.
 */
public class Controller {
	
	private Sale sale;
	private Printer printer;
	private SaleObserver saleObserver;
	
	/**
	 * Constructor
	 * @param prntr The printer object initiated in main method.
	 */
	public Controller(Printer prntr){
		printer = prntr;
	}
	
	/**
	 * Initiate a new instance of a sale and sets 
	 * the saleObserver for the sale instance.
	 */
	public void initSale() {
		sale = new Sale(printer);
		sale.setSaleObserver(saleObserver);
	}
	
	/**
	 * Returns the total cost for the customer.
	 * @return totals
	 */
	public double endSale() {
		return sale.endSale();
	}
	
	/**
	 * Adds an item with its quantity to the sale.
	 * @param itemId	unique item identifier
	 * @param quantity	amount of the item to register.
	 * @return the item object added to the sale.
	 */
	public ItemDTO addItemToSale(int itemId, int quantity) throws ItemNotFoundException, OperationFailedException{
		
		if(sale == null){
			throw new IllegalStateException("No sale active. Start sale before entering items.");
		}
			
		return sale.addItem(itemId, quantity);
	}
	
	/**
	 * Register paid amount by the customer and
	 * returns the change if the payment covers
	 * the total cost. Prints receipt.
	 * @param amount
	 * @return change to give to the customer.
	 */
	public double pay(double amount){
		return sale.pay(amount);
	}
	
	
	/**
	 * Sets the observer.
	 * @param observer
	 */
	public void addSaleObserver(SaleObserver observer) {
		saleObserver = observer;
	}
}
