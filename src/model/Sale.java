package model;

import integration.*;
import java.sql.SQLException;

/*
 * Handling all logic regarding a sale.
 */
public class Sale {
	
	private SaleDTO salesData;
	private Printer printer;
	private Payment payment;
	private Receipt receipt;
	private ItemCatalog itemCatalog; 
	private SaleObserver saleObserver;
	
	/**
	 * Constructor for Sales class.
	 * @param prntr object of class Printer.
	 */
	public Sale(Printer prntr){
		printer = prntr;
		payment = new Payment();
		itemCatalog = new ItemCatalog();
		salesData = new SaleDTO();
	}
	
	/**
	 * 
	 * Searches for item in item catalog with given identifier
	 * and adds it to the salesData and then returns the item.
	 * 
	 * @param itemIdentifier 	Unique identifier for the item. 
	 * @param quantity       	How many of the item should be registered.
	 * @return ItemDTO			The item that was added to the sale.
	 * @throws ItemNotFoundException
	 * @throws OperationFailedException
	 */
	public ItemDTO addItem(int itemIdentifier, int quantity) throws ItemNotFoundException, OperationFailedException {
		
		try {
			ItemDTO item = itemCatalog.search(itemIdentifier);
		
			salesData.addItem(item, quantity);
			payment.addItem(item, quantity);
			
			return item;
		}
		catch (SQLException sqlException) {
			throw new OperationFailedException("Database error.", sqlException);
		}
	}
	
	/**
	 * Getting totals cost for sale.
	 * @return total cost that the user should pay.
	 */ 
	public double endSale() {
		return payment.getTotals();
	}
	
	/**
	 * Calculates the change and prints the receipt, 
	 * as well as logging the sale. 
	 * @param paidAmount	The amount of cash the the customer
	 * 						gave to the cashier.
	 * @return change that the user should be given.
	 * If -1 is returned, the user didn't pay enough.
	 */
	public double pay(double paidAmount) {
		
		double change = payment.getChange(paidAmount);
		
		if(change >= 0) {
			
			salesData.setPaid(paidAmount);
			salesData.setChange(change);
			
			receipt = new Receipt(salesData);
			printer.printReceipt(receipt);
			
			notifyObserver();
			
			itemCatalog.logSale(salesData);
			
			return change;
		}
		
		return -1;
	}
	
	
	/**
	 * Notifies observer when state change. 
	 */
	private void notifyObserver() {
		saleObserver.updateTotalSales(payment.getTotals());
	}
	
	/**
	 * Setter for observer. 
	 * @param observer to set. 
	 */
	public void setSaleObserver (SaleObserver observer) {
		this.saleObserver = observer;
	}
}


