package model;

import java.util.*;
import java.text.*;

import java.util.ArrayList;
import integration.ItemDTO;

/*
 * Class that stores information about a Sale.
 */
public class SaleDTO {
	
	private double totals;
	private double taxes;
	private double paid;
	private double change;
	private ArrayList<ItemDTO> items;

	/**
	 * Constructor
	 */
	public SaleDTO(){
		totals = 0;
		taxes = 0;
		paid = 0;
		change = 0;
		items = new ArrayList<ItemDTO>();
	}
	
	/**
	 * Adds item cost plus  taxes to totals and taxes
	 * variables and adds the item to the item list.
	 * @param item	ItemDTO scanned by the system.
	 */
	public void addItem(ItemDTO item, int quantity) {
		
		for(int i = 0; i < quantity; i++) {
			totals += (item.getPrice() + item.getPrice() * item.getTaxRate());
			taxes += (item.getPrice() * item.getTaxRate());
			items.add(item);
		}
	}
	
	/**
	 * Returns array with all registered items.
	 * @return items
	 */
	public ArrayList<ItemDTO> getItems(){
		return items;
	}
	
	/**
	 * Sets paid amount by the user
	 * @param paidAmount
	 */
	public void setPaid(double paidAmount) {
		paid = paidAmount;
	}
	
	/**
	 * Sets change
	 * @param changeAmount
	 */
	public void setChange(double changeAmount) {
		change = changeAmount;
	}
	
	/**
	 * Converts sales info to readable string.
	 * @return sales info string.
	 */
	public String itemsToString(){
		StringBuilder itemsString = new StringBuilder();
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss\n");
        itemsString.append("\nDate and time of purchase: \n"+dateFormat.format(date));
        
		itemsString.append("\nPurchased items:\n");
		for(ItemDTO item : items) {
			itemsString.append(item.toString() + "\n");
		}
		
		itemsString.append("\nTOTALS: " + totals);
		
		itemsString.append("\n\nINCLUDED TAXES: " + taxes);
		
		itemsString.append("\n\nPAYED BY CUSTOMER: " + paid);
		
		itemsString.append("\n\nCHANGE TO CUSTOMER: " + change);
	
		return itemsString.toString();
	}
}


