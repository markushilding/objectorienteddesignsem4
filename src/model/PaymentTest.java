package model;

import static org.junit.Assert.*;
import org.junit.Test;

import integration.ItemDTO;

/*
 * Testing methods in Payment class.
 */
public class PaymentTest {
	
	private Payment payment;		
	
	
	@Test
	public void testGetTotals() {
		
		payment = new Payment();
		
		SaleDTO data = new SaleDTO();
		
		ItemDTO item1 = new ItemDTO(1, "Apple", 10, 0.25);
		ItemDTO item2 = new ItemDTO(2, "Orange", 20, 0.25);
		
		data.addItem(item1, 1);
		data.addItem(item2, 1);
		
		payment.addItem(item1, 1);
		payment.addItem(item2, 1);
		
		assertEquals(37.5, payment.getTotals(), 0);
	}
	
	
	@Test
	public void testGetChange() {
		
		payment = new Payment();
		
		SaleDTO data = new SaleDTO();
		
		ItemDTO item1 = new ItemDTO(1, "Apple", 10, 0.25);
		ItemDTO item2 = new ItemDTO(2, "Orange", 20, 0.25);
		
		data.addItem(item1, 1);
		data.addItem(item2, 1);
		
		payment.addItem(item1, 1);
		payment.addItem(item2, 1);
		
		assertEquals(2.5, payment.getChange(40), 0);
	}
}
