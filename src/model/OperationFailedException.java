package model;

public class OperationFailedException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param msg
	 * @param cause
	 */
	public OperationFailedException(String msg,	Exception cause) {
		super(msg, cause);
	}
}