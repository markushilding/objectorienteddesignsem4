package model;



import static org.junit.Assert.*;
import org.junit.Test;

import integration.*;
import view.*;
/*
 * Testing methods in Sale class.
 */
public class SaleTest {

	/**
	 * Testing addItem function with a item that
	 * exists in item catalog and should therefore
	 * pass in try statement.
	 */
	@Test
	public void testAddItemGoodItem() {
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		ItemDTO item = new ItemDTO(1, "Banana", 8.99, 0.12);
		
		try {
			assertEquals(item.getName(), sale.addItem(1, 1).getName());
		}
		catch(ItemNotFoundException infException) {
			fail("ItemNotFoundException occurred.");
		}
		catch(OperationFailedException ofException) {
			fail("OperationFailedException occurred.");
		}
	}
	
	/**
	 * Testing addItem function with an item
	 * that not exists and should therefore
	 * result in a ItemNotFoundExceptio. 
	 * The test passed if that exception
	 * is raised.
	 */
	@Test
	public void testAddItemBadItem() {
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		
		try {
			assertEquals(null, sale.addItem(-55, 1));
		}
		catch(ItemNotFoundException infException) {
			return;
		}
		catch(OperationFailedException ofException) {
			fail("OperationFailedException occurred.");
		}
	}
	
	/**
	 * Test for ending the sale.
	 * Should return the totals of buying
	 * item with ID 9, which is 62,5.
	 */
	@Test
	public void testEndSale() {
		Printer printer = new Printer();
		Sale sale = new Sale(printer);

		try {
			sale.addItem(9, 1);
		}
		catch(ItemNotFoundException infException) {
			fail("ItemNotFoundException occurred.");
		}
		catch(OperationFailedException ofException) {
			fail("OperationFailedException occurred.");
		}
			
		assertEquals(62.5, sale.endSale(), 0);
	}
	
	/**
	 * Testing pay function with good payment
	 * and bad payment (to little money). 
	 */
	@Test
	public void testPay(){
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		
		sale.setSaleObserver(new TotalRevenueView());
		
		try {
			sale.addItem(9, 1);
		}
		catch(ItemNotFoundException infException) {
			fail("ItemNotFoundException occurred.");
		}
		catch(OperationFailedException ofException) {
			fail("OperationFailedException occurred.");
		}
		
		double a = sale.pay(70);
		double b = sale.pay(10);
		
		assertEquals(7.5, a, 0);
		assertEquals(-1, b, 0);
	
	}
}
