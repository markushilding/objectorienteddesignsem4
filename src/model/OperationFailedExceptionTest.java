package model;

import static org.junit.Assert.*;
import org.junit.Test;

import integration.ItemDTO;
import integration.ItemNotFoundException;
import integration.Printer;


public class OperationFailedExceptionTest {
	
	@Test
	public void testException() throws ItemNotFoundException, OperationFailedException{
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		
		try {
			ItemDTO i = sale.addItem(7, 1);
		}
		catch(OperationFailedException ofException) {
			assertTrue(ofException.getMessage().contains("Database error"));
		}
		catch(ItemNotFoundException infException) {
			fail("ItemNotFoundException occurred");
		}
		
		fail("Try statement should fail with OperationFailedException.");
	}
	
}
