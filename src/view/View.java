package view;

import integration.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import controller.*;
import model.*;



/*
 * Handling user interactions and passes it to the controller
 * as well as printing messages with transaction information.
 */
public class View {

	private final Controller controller;
	
	/**
	 * Constructor
	 * @param controller 	Controller for the program.
	 */
	public View(Controller controller) {
		this.controller = controller;
		controller.addSaleObserver(new TotalRevenueView());
	}
	
	/**
	 * Hard coded interaction flow for processing a sale.
	 * @return
	 */
	public boolean flow(){
		
		
			
		// First instance
		initSale();
		
		enterItem(1, 1);
		enterItem(2, 1);
		enterItem(3, 1);
		enterItem(4, 1);
		
		System.out.println("\n* Simulating non existing item. *");
		enterItem(100, 1);
		
		System.out.println("\n* Simulating non database error by adding item with id 7 . *");
		enterItem(7, 1);
		
		endSale();
		
		pay(100);
		
		
		//Second instance
		initSale();
		
		enterItem(1,1);
		enterItem(5,1);
		
		endSale();
		
		pay(200);
		
		return false;
	}
	
	/**
	 * View function for starting a new sale
	 * with the controller.
	 */
	public void initSale(){
		controller.initSale();
		System.out.println("\n* New sale initiated. *\n");
	}
	
	/**
	 * View function for entering an item and handling the data
	 * returned from the controller and prints it.
	 * @param itemIdentifier
	 * @param quantity
	 */
	public void enterItem(int itemIdentifier, int quantity) {
		
		try {
			ItemDTO item = controller.addItemToSale(itemIdentifier, quantity);
			System.out.println(quantity + " x " + item.getName() + " was added to sale.");
		}
		catch(ItemNotFoundException infException){
			System.out.println("Item with id " + itemIdentifier + " not found.");
	
			logException(infException.getMessage(), infException);

		}
		catch(OperationFailedException ofException){
			System.out.println("Database error occured.");
			
			logException(ofException.getMessage(), ofException);
		}
	}
	
	/**
	 * View function for ending the sale and handling data
	 * returned by the controller and prints it.
	 */
	public void endSale() {
		
		double totals = controller.endSale();
		System.out.println("Totals with taxes: " + totals); 
	}
	
	/**
	 * View function for pay, handling return data
	 * from the controller and prints it.
	 * @param amount
	 */
	public void pay(double amount) {
		
		double change = controller.pay(amount);
		
		if(change == -1) {
			System.out.println("Paid amount is to low."); 
		}
		else {
			System.out.println("Payment ok.");
		}
	}
	
	
	/**
	 * 
	 * @param message 	Custom exception message 
	 * @param e			The exception
	 */
	public void logException(String message, Exception e) {
			
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		System.out.println("\n* - - Intended for developers (log) - - *");
		
		System.out.println("\nTimestamp for error: "+dateFormat.format(date));
		
		System.out.println("\nError message: \n" + message + "\n");

		System.out.println("Stack trace:");
		e.printStackTrace();
		
		System.out.println("* - - - - - - - - - - - - - - - - - - - *\n");
	}
}
