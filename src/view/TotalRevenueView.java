package view;

import model.SaleObserver;

/**
 * Observer class of total sales.
 * Prints the sum of all sales during the
 * lifetime of the program.
 */
public class TotalRevenueView implements SaleObserver {
	
	private double totalSalesSum;
	
	/**
	 * Updates the total sales sum for the observer.
	 * @param amount totals for a sale. 
	 */
	@Override
	public void updateTotalSales(double amount) {
		totalSalesSum += amount;
		System.out.println("\nDISPLAY: " + totalSalesSum);
	}
}